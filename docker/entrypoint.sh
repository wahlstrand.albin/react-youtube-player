#!/bin/sh

# Ensures container will only start with the required environment variables
[ -z "$API_KEY" ] && echo "API_KEY needs to be added to the environment" && exit 1;

# Removes default config file
rm /etc/nginx/conf.d/default.conf

# Replaces environment variables in config template and stores it in the configuration path
envsubst '\$API_KEY \$PORT' < /app/nginx.conf.template > /etc/nginx/conf.d/ryp.conf

# Setting entrypoint
exec nginx -g 'daemon off;'