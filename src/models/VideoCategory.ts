export interface VideoCategory {
    title: string;
    id: string;
}
