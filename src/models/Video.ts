export interface Video {
    title: string;
    id: string;
    description: string;
    thumbnails: ThumbNails;
    playSrc: string;
  }
  
  export interface ThumbNails {
      default: Thumbnail;
      high: Thumbnail;
      maxres: Thumbnail;
      medium: Thumbnail;
      standard: Thumbnail;
  }
  
  export interface Thumbnail {
      url: string;
      width: number;
      height: number;
  }
  