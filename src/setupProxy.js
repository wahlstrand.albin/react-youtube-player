const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  if (process.env.NODE_ENV === 'development')  {
    app.use(
      '/api',
      createProxyMiddleware({
        target: 'https://www.googleapis.com/youtube/v3',
        changeOrigin: true,
        pathRewrite: (path) => {
          let newPath = path.replace("/api", "");
          const keyValue = process.env.REACT_APP_YOUTUBE_KEY;
          newPath += `&key=${keyValue}`;
  
          return newPath;
        }
      })
    );
  }
};