import { useState } from 'react';

import './App.scss';

import { VideoSelector } from './components/VideoSelector/VideoSelector';
import { FilterMenu } from './components/FilterMenu/FilterMenu';

export const App = () => {
  const [filterId, setFilterId] = useState<string>('');

  const onFilterClick = (event: any) => {
    setFilterId(event.target.value);
  };

  return <div className="App">
    <header className="App-header">
      <FilterMenu onFilterSelected={onFilterClick}></FilterMenu>
    </header>
    <section className="App-section">
      <VideoSelector filterId={filterId}></VideoSelector>
    </section>
  </div>;
};

export default App;
