import './VideoItem.scss';

import { Video } from '../../models/Video';

export interface VideoSelectorProps {
    video: Video;
    onVideoSelected: (video: Video) => void;
    isCurrentlySelected: boolean;
}

export const VideoItem = ({video, onVideoSelected, isCurrentlySelected}: VideoSelectorProps) => {
    const onVideoClick = () => {
        onVideoSelected(video);
    };

    return (
        <div className={`VideoItem-Container ${isCurrentlySelected ? "Selected" : ""}`} onClick={onVideoClick}>
            <img className="VideoItem-Thumbnail" alt="Video thumbnail" src={video.thumbnails?.standard?.url || video.thumbnails?.medium?.url }/>
            <section className="VideoItem-Info">
                <span className="VideoItem-Title" title={video.title}>{video.title}</span>
                <p className="VideoItem-Description">{video.description}</p>
            </section>
        </div>);
}
