import { useEffect, useState } from 'react';
import axios from 'axios';

import './VideoSelector.scss';

import { PaginationTokens, Paginator } from '../Paginator/Paginator';
import { SelectedVideo } from '../SelectedVideo/SelectedVideo';
import { Video } from '../../models/Video';
import { VideoItem } from '../VideoItem/VideoItem';

export interface VideoSelectorProps {
    filterId: string;
}

export interface VideoSelectorState {
    currentlySelected: string[];
}

export const VideoSelector = ({filterId}: VideoSelectorProps) => {
    const [selectedVideo, setSelectedVideo] = useState<Video | undefined>();
    const [displayedVideos, setDisplayedVideos] = useState<Video[]>([]);
    const [paginationTokens, setPaginationTokens] = useState<PaginationTokens>();

    useEffect(() => {
        if (filterId) {
            setSelectedVideo(undefined);
            fetchVideos(filterId);
        }
    }, [filterId]);

    const fetchVideos = (filter?: string, pageToken?: string) => {
        const apiCall = `/api/videos?part=snippet%2CcontentDetails%2Cstatistics&chart=mostPopular&maxResults=10&regionCode=AE&${pageToken ? `pageToken=${pageToken}` : ""}&videoCategoryId=${filter}`;
    
        axios.get(apiCall)
            .then((response: any) => {
                const videos = response.data.items.map((item: any) => ({
                    title: item.snippet.title,
                    id: item.id,
                    thumbnails: item.snippet.thumbnails,
                    description: item.snippet.description,
                    playSrc: `https://www.youtube.com/embed/${item.id}`
                } as Video));

                setDisplayedVideos(videos);
                setPaginationTokens({ nextPageToken: response.data.nextPageToken, previousPageToken: response.data.prevPageToken });
            });
    }

    const closeClick = () => {
        setSelectedVideo(undefined);
    };

    const onNextClick = (token: string) => {
        fetchVideos(filterId, token);
    };

    const onPreviousClick = (token: string) => {
        fetchVideos(filterId, token);
    };

    return (<div className="VideoSelector-Container">
        {
            displayedVideos.map((video, index) => (
                (selectedVideo?.id === video.id)
                        ? <SelectedVideo key={index} video={selectedVideo} onCloseClick={closeClick}></SelectedVideo>
                        : <VideoItem key={index} video={video} onVideoSelected={setSelectedVideo} isCurrentlySelected={selectedVideo?.id === video.id}></VideoItem>
                
                )
            )
        }
        <section className="VideoSelector-Footer">
            {
                paginationTokens ? <Paginator pageTokens={paginationTokens} next={onNextClick} previous={onPreviousClick}></Paginator> : ""
            }
        </section>
    </div>);
}
