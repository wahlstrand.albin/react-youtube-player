import { X } from 'react-bootstrap-icons';

import './SelectedVideo.scss'

import { Video } from '../../models/Video';

export interface SelectedVideoProps {
    video: Video;
    onCloseClick: (video: Video) => void;
}

export const SelectedVideo = ({video, onCloseClick}: SelectedVideoProps) => {
    const closeClick = () => {
        onCloseClick(video);
    }

    return (<div className={`SelectedVideo-Container`}>
        <section className="SelectedVideo-TopSection">
            <X className="SelectedVideo-CloseButton" onClick={closeClick}></X>
        </section>
        <iframe className="SelectedVideo-Player" src={video.playSrc} allowFullScreen title="Video player" allow="encrypted-media"></iframe>
        <section className="SelectedVideo-Info">
            <span className="SelectedVideo-Title">{video.title}</span>
            <p className="SelectedVideo-Description">{video.description}</p>
        </section>
    </div>);
}
