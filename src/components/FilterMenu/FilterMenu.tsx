import { useEffect, useState } from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Button from 'react-bootstrap/Button';
import axios from 'axios';

import { VideoCategory } from '../../models/VideoCategory';

export interface FilterMenuProps {
    onFilterSelected: (event: any) => void;
}

export interface FilterMenuState {
    filters: string[];
}

export const FilterMenu = ({onFilterSelected}: FilterMenuProps) => {
    const [filters, setFilters] = useState<VideoCategory[]>([]);

    useEffect(() => {
        const includedFilters = [
            'Music', 
            'Sports', 
            'Gaming', 
            'Pets & Animals', 
            'Film & Animation'
        ];

        //Fetch available filters
        axios.get('/api/videoCategories?part=snippet&regionCode=AE')
            .then((response: any) => {
                const videoFilters = [];
                const items = response.data.items;

                for (const item of items) {
                    if (includedFilters.includes(item.snippet.title)) {
                        videoFilters.push({
                            title: item.snippet.title,
                            id: item.id
                        } as VideoCategory);
                    }
                }
                setFilters(videoFilters);
            }
        );
    }, []);

    return (<ButtonGroup aria-label="Basic example">
                 {filters.map((filter, index) => (
                     <Button variant="secondary" key={index} value={filter.id} onClick={onFilterSelected}>{filter.title}</Button>
                 ))}
             </ButtonGroup>);
}
