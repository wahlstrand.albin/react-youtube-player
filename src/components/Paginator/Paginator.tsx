import { ArrowLeft, ArrowRight } from 'react-bootstrap-icons';

import "./Paginator.scss";

export interface PaginatorProps {
    pageTokens: PaginationTokens | undefined;
    previous: (pageToken: string) => void;
    next: (pageToken: string) => void;
}

export interface PaginationTokens {
    nextPageToken: string;
    previousPageToken: string;
}

export const Paginator = ({pageTokens, previous, next}: PaginatorProps) => {
    const previousClick = () => {
        if (pageTokens) {
            previous(pageTokens.previousPageToken);
        }
    };
    
    const nextClick = () => {
        if (pageTokens) {
            next(pageTokens.nextPageToken);
        }
    };

    return (<div className="Paginator-Container">
        {
            pageTokens?.previousPageToken ? <span title="Previous"><ArrowLeft className="Paginator-Previous" onClick={previousClick}></ArrowLeft></span> : ""
        }
        {
            pageTokens?.nextPageToken ? <span title="Next"><ArrowRight className="Paginator-Next" onClick={nextClick}></ArrowRight></span> : ""
        }
    </div>);
}
