# React Youtube player

This is a POC for creating a simple youtube application using React & Typescript.

## Running the UI

The application requires a youtube API key to be set in the `.env` file in the base of the project.
Once this is provided, run the command `npm install && npm run start` to start the application.

Navigate to `http://localhost:4200/` and the app will present you a list with the available filters.

## Testing

No tests where written during the development of this project.

## Building container

You can build the container by running the command `npm install && npm run build` to create the build folder - followed
by `npm run docker:build` to build the docker image.

## Running container

Running the docker container requires a youtube API key to be set in the `.docker-compose` file in the base of the project.
Once this is done, run the command `docker-compose up` to start the container, which will then be accessible from `http://localhost:4205/`.

## Assets

Icons are supplied by `react-bootstrap-icons`.